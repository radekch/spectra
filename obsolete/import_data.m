%% import avg reflection

filename = 'C:\Users\Radek\Google Drive\Postdoc\1p-2p scope\Spectra\Avg Reflection.txt';
delimiter = '\t';
startRow = 14;
formatSpec = '%f%f%[^\n\r]';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'HeaderLines' ,startRow-1, 'ReturnOnError', false);
fclose(fileID);

l_ravg = dataArray{:, 1};
ravg = dataArray{:, 2};
clearvars filename delimiter startRow formatSpec fileID dataArray ans;

plot(l_ravg,ravg)
ylabel('Reflection')
xlabel('\lambda (nm)')
%% import gfp emission

filename = 'C:\Users\Radek\Google Drive\Postdoc\1p-2p scope\Spectra\GFP emission.txt';
delimiter = '\t';
startRow = 4;
formatSpec = '%f%f%[^\n\r]';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'HeaderLines' ,startRow-1, 'ReturnOnError', false);
fclose(fileID);

lgfp_em = dataArray{:, 1};
gfp_em = dataArray{:, 2};
lgfp_em=lgfp_em(1:(end-1));
gfp_em=gfp_em(1:(end-1));
gfp_em_norm=gfp_em/sum(gfp_em);
clearvars filename delimiter startRow formatSpec fileID dataArray ans;

plot(lgfp_em,gfp_em)
ylabel('Emission (a.u.)')
xlabel('\lambda (nm)')

%% import gfp absorbtion

filename = 'C:\Users\Radek\Google Drive\Postdoc\1p-2p scope\Spectra\GFP absorption.txt';
delimiter = '\t';
startRow = 4;
formatSpec = '%f%f%[^\n\r]';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'HeaderLines' ,startRow-1, 'ReturnOnError', false);
fclose(fileID);

lgfp_abs = dataArray{:, 1};
gfp_abs = dataArray{:, 2};
lgfp_abs=lgfp_abs(1:(end-1));
gfp_abs=gfp_abs(1:(end-1));
gfp_abs_norm=gfp_abs/sum(gfp_abs);
clearvars filename delimiter startRow formatSpec fileID dataArray ans;

plot(lgfp_abs,gfp_abs)
ylabel('Absorbtion (a.u.)')
xlabel('\lambda (nm)')
%% import collection filter transmission

filename = 'C:\Users\Radek\Google Drive\Postdoc\1p-2p scope\Spectra\FF02-525_40_Spectrum.txt';
delimiter = '\t';
startRow = 5;
formatSpec = '%f%f%[^\n\r]';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'HeaderLines' ,startRow-1, 'ReturnOnError', false);
fclose(fileID);

l_tcoll = dataArray{:, 1};
tcoll = dataArray{:, 2};
clearvars filename delimiter startRow formatSpec fileID dataArray ans;

plot(l_tcoll,tcoll)

%% all spectra together
plot(lgfp_em,gfp_em,'g-',lgfp_abs,gfp_abs,l_ravg,ravg,l_tcoll,tcoll,'LineWidth',2)
xlabel('\lambda (nm)')
xlim([400, 600])
legend('Emission','Absorption','1p/2p dichroic NFD01-532, reflection',...
    'Collection filter FF02-525, transmition','location','northwest')
% ylabel('Emission (a.u.)')
saveas(gcf,'C:\Users\Radek\Google Drive\Postdoc\1p-2p scope\Spectra\allspectra.pdf')

%% data interpolation
ref_interp = interp1(l_ravg,ravg,lgfp_em,'spline');
trans_interp = interp1(l_tcoll,tcoll,lgfp_em,'spline');

plot(lgfp_em,gfp_em_norm,'g',lgfp_em,ref_interp.*gfp_em_norm,lgfp_em,trans_interp.*gfp_em_norm,'LineWidth',2)
xlim([480,700])
ref_total=sum(ref_interp.*gfp_em_norm);
trans_total=sum(trans_interp.*gfp_em_norm);
xlabel('\lambda (nm)')
ylabel('Filtered emission spectrum')
legend('GFP emission',sprintf('1p/2p dichroic NFD01-532, total reflection: %.0f%%',ref_total*100),...
    sprintf('Collection filter FF02-525, total transmission: %.0f%%',trans_total*100))
ylim([0,max(gfp_em_norm)*1.02])

saveas(gcf,'C:\Users\Radek\Google Drive\Postdoc\1p-2p scope\Spectra\filtered_spectra.pdf')