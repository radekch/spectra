classdef Spectra < handle
    % class evaluating spectra properties of filters and sources for
    % TEMPO experiments
    
    events
        PropertyChange
    end
    
    
    properties (SetObservable=true) % there will be an event if they change
        Fluophor1
        Fluophor2
        
        LED1
        LED2
        
        Exciter1
        Exciter2
        
        Emitter1
        Emitter2
        
        Dichroic1
        Dichroic2
        
    end
    
    properties % they will be evaluated later on
        
        Collection_Efficiency_Signal
        Collection_Efficiency_Rerefence
        
        Excitation_Efficiency_Signal_LED1
        Excitation_Efficiency_Reference_LED1
        Excitation_Efficiency_Signal_LED2
        Excitation_Efficiency_Reference_LED2
        
        LED1_power_trans
        LED2_power_trans
        
        LED1_bleed2signal
        LED1_bleed2reference
        LED2_bleed2signal
        LED2_bleed2reference
        
        Crosstalk_Reference2Signal
        Crosstalk_Signal2Reference
        
        data
        
        lambda_range=[400,700]
        
        configuration
        
        
        
    end
    
    properties (Access=public)
        listeners % struct of handles to all evoked listeners
    end
    
    
    methods
        function obj=Spectra()
            % instead of loading data explicitly by constructor arguments, the properties will be passed by properties
            % and their changed will be listen by listeners which elicit
            % subsequent action.
            
            createLisn(obj)
            
        end % constructor
        
        function createLisn(obj) % create listeners
            
            
            
            obj.listeners.Fluophor1 = addlistener(obj,'Fluophor1','PostSet',@(src,evnt)react2listener(obj,src,evnt));
            obj.listeners.Fluophor2 = addlistener(obj,'Fluophor2','PostSet',@(src,evnt)react2listener(obj,src,evnt));
            
            obj.listeners.LED1 = addlistener(obj,'LED1','PostSet',@(src,evnt)react2listener(obj,src,evnt));
            obj.listeners.LED2 = addlistener(obj,'LED2','PostSet',@(src,evnt)react2listener(obj,src,evnt));
            
            obj.listeners.Exciter1 = addlistener(obj,'Exciter1','PostSet',@(src,evnt)react2listener(obj,src,evnt));
            obj.listeners.Exciter2 = addlistener(obj,'Exciter2','PostSet',@(src,evnt)react2listener(obj,src,evnt));
            
            obj.listeners.Emitter1 = addlistener(obj,'Emitter1','PostSet',@(src,evnt)react2listener(obj,src,evnt));
            obj.listeners.Emitter2 = addlistener(obj,'Emitter2','PostSet',@(src,evnt)react2listener(obj,src,evnt));
            
            obj.listeners.Dichroic1 = addlistener(obj,'Dichroic1','PostSet',@(src,evnt)react2listener(obj,src,evnt));
            obj.listeners.Dichroic2 = addlistener(obj,'Dichroic2','PostSet',@(src,evnt)react2listener(obj,src,evnt));
            
            
        end
        
        function react2listener(obj,src,evnt)
            newpropval=obj.(src.Name);
            
            if ~isempty(newpropval)
                
                if length(newpropval)>3
                    if strcmp(newpropval(end-3:end),'.txt')
                        newpropval=newpropval(1:end-4);
                    elseif strcmp(newpropval(end-2:end),'txt')
                        newpropval=newpropval(1:end-3);
                    end
                end

                proptype=src.Name(1:3);
                
                switch proptype
                    case 'Flu'
                        folder='fluophor';
                        [filename_abs,filepath_abs]=obj.getFileName(folder,[newpropval,'*Abs*.txt']);
                        [filename_em,filepath_em]=obj.getFileName(folder,[newpropval,'*Em*.txt']);
                        obj.(src.Name)=filename_abs(1:end-10);
                        
                        absorbtion=obj.importfile(filepath_abs);
                        emission=obj.importfile(filepath_em);
                        
                        obj.data.(src.Name).Abs=absorbtion;
                        obj.data.(src.Name).Em=emission;
                        
                    case 'LED'
                        folder='source';
                        [filename,filepath]=obj.getFileName(folder,newpropval);
                        obj.data.(src.Name)=obj.importfile(filepath);
                        obj.(src.Name)=filename(1:end-4);
                    case 'Exc'
                        folder='exciter';
                        [filename,filepath]=obj.getFileName(folder,newpropval);
                        obj.data.(src.Name)=obj.importfile(filepath);
                        obj.(src.Name)=filename(1:end-4);
                    case 'Emi'
                        folder='emitter';
                        [filename,filepath]=obj.getFileName(folder,newpropval);
                        obj.data.(src.Name)=obj.importfile(filepath);
                        obj.(src.Name)=filename(1:end-4);
                    case 'Dic'
                        folder='dichroic';
                        [filename,filepath]=obj.getFileName(folder,newpropval);
                        obj.data.(src.Name)=obj.importfile(filepath); 
                        obj.(src.Name)=filename(1:end-4);
                end
                               
            end
            
            recalculate_properties(obj);
            
        end
        
        recalculate_properties(obj)      
        plot(obj,varargin)

        
    end %methods
    
    methods (Static)
        spectrum = importfile(filename, startRow, endRow)
        [spectrum,overlap]=spectr_multpl(spectrum1,spectrum2,varargin)
        
        function spectrum_out=trans2reflect(spectrum_in)
            spectrum_out=spectrum_in;
            spectrum_out(:,2)=1-spectrum_out(:,2);
        end
        plot_spectr(spectrum)
        
        output_spectrum=trans_multpl(spectrum1,spectrum2,varargin)
        
        
    end % methods (Static)
    
    methods (Static,Access=private)
        function [filename,filepath]=getFileName(folder,pattern)
            % function returns the name and path to the first file in the folder
            % from the search according to the specified pattern e.g.
            % '*.txt'
            % If no files were founded, emty string are  returned.
            filelist=dir([folder,'\',pattern]);
            
            if isempty(filelist)
                filename='';
                filepath='';
            else
                if numel(filelist)>1
                    fprintf('\n%i files in folder %s matches the pattern %s\n',numel(filelist),folder,pattern);
                end
                filename=filelist(1).name;
                filepath=[folder,'\',filename];
            end
        end
        
        
        
    end % methods (Static,Access=private)
    
end % classdef