clf
plot(LED_lambda,LED_em,'LineWidth',2)
hold on
plot(alluxa(:,1),0.01*alluxa(:,2),'LineWidth',2)
plot(excfilter_lambda,excfilter_t,'LineWidth',2)
plot(lgfp_abs,gfp_abs,'LineWidth',2)
plot(lgfp_em,gfp_em)
xlim([300,600])
hold off

%% transmission - previous calculations

ref_interp = interp1(l_ravg,ravg,lgfp_em,'spline');
trans_interp = interp1(l_tcoll,tcoll,lgfp_em,'spline');

ref_total=sum(ref_interp.*gfp_em_norm);
trans_total=sum(trans_interp.*gfp_em_norm);

%% 

interp1(lgfp_abs,gfp_abs,LED_lambda)

%% lambda

lambda=linspace(350,600,1000);

gfpabs_new=interp1(lgfp_abs,gfp_abs,lambda);
LEDem_new=interp1(LED_lambda,LED_em,lambda);
alluxa_new=interp1(alluxa(:,1),0.01*alluxa(:,2),lambda);
exc_new=interp1(excfilter_lambda,excfilter_t,lambda);

clf
hold on
plot(lambda,gfpabs_new)
plot(lambda,LEDem_new)
plot(lambda,alluxa_new)
plot(lambda,exc_new)
plot(lambda,LEDem_new.*exc_new.*alluxa_new,'--','LineWidth',2)

%%
clf
plot(lambda,LEDem_new.*gfpabs_new.*alluxa_new)

sum(LEDem_new.*exc_new.*alluxa_new)/sum(LEDem_new)

%% blead through

sum(LEDem_new.*exc_new.*(1-alluxa_new))/sum(LEDem_new)


