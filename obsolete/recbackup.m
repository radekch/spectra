function recalculate_properties(obj)
% recalculate properties based on freshly loaded data for filters etc.
% function recognizes and updates the current configurations of the set

if isempty(obj.Fluophor1)&&isempty(obj.Fluophor2)
    return
end

props_loaded=~isempty(obj.Fluophor1)&~isempty(obj.Fluophor2)&~isempty(obj.LED1)&~isempty(obj.LED2)&...
    ~isempty(obj.Exciter1)&~isempty(obj.Emitter1)&~isempty(obj.Emitter1)&~isempty(obj.Emitter2)&...
    ~isempty(obj.Dichroic1)&~isempty(obj.Dichroic2);



%%%%% Colection efficiency & crosstalks
if props_loaded
    if isempty(obj.Dichroic1)&&isempty(obj.Dichroic2)
        [~,obj.Collection_Efficiency_Signal]=obj.spectr_multpl(obj.data.Fluophor1.Em,obj.data.Emitter1,obj.lambda_range);

        [~,obj.Collection_Efficiency_Rerefence]=obj.spectr_multpl(obj.data.Fluophor2.Em,obj.data.Emitter2,obj.lambda_range);
        
        [~,obj.Crosstalk_Reference2Signal]=obj.spectr_multpl(obj.data.Fluophor2.Em,obj.data.Emitter1,obj.lambda_range);
        [~,obj.Crosstalk_Signal2Reference]=obj.spectr_multpl(obj.data.Fluophor1.Em,obj.data.Emitter2,obj.lambda_range);
    else

        refdichroic2=obj.trans2reflect(obj.data.Dichroic2);
        emitter1dichroics=obj.trans_multpl(obj.data.Emitter1,obj.data.Dichroic1,obj.lambda_range);
        emitter1dichroics=obj.trans_multpl(emitter1dichroics,refdichroic2,obj.lambda_range);
        
        emitter2dichroics=obj.trans_multpl(obj.data.Emitter2,obj.data.Dichroic1,obj.lambda_range);
        emitter2dichroics=obj.trans_multpl(emitter2dichroics,obj.data.Dichroic2,obj.lambda_range);
        
        [~,obj.Collection_Efficiency_Signal]=obj.spectr_multpl(obj.data.Fluophor1.Em,emitter1dichroics,obj.lambda_range);
        [~,obj.Collection_Efficiency_Rerefence]=obj.spectr_multpl(obj.data.Fluophor2.Em,emitter2dichroics,obj.lambda_range);
        
        [~,obj.Crosstalk_Reference2Signal]=obj.spectr_multpl(obj.data.Fluophor2.Em,emitter1dichroics,obj.lambda_range);
        [~,obj.Crosstalk_Signal2Reference]=obj.spectr_multpl(obj.data.Fluophor1.Em,emitter2dichroics,obj.lambda_range);   
    end
    
end

%%%% LED power transmitted

if props_loaded
    if isempty(obj.Dichroic1)
        [filtered_LED1,obj.LED1_power_trans]=obj.spectr_multpl(obj.data.LED1,obj.data.Exciter1,obj.lambda_range);
        [filtered_LED2,obj.LED2_power_trans]=obj.spectr_multpl(obj.data.LED2,obj.data.Exciter1,obj.lambda_range);
    else
        % dichoric1 filtering:
        dichroic1ref=obj.trans2reflect(obj.data.Dichroic1);
        dichroic1exc1=obj.trans_multpl(dichroic1ref,obj.data.Exciter1,obj.lambda_range);
        
        [filtered_LED1,obj.LED1_power_trans]=obj.spectr_multpl(obj.data.LED1,dichroic1exc1,obj.lambda_range);
        [filtered_LED2,obj.LED2_power_trans]=obj.spectr_multpl(obj.data.LED2,dichroic1exc1,obj.lambda_range);
    end
end



%%%%% Excitation efficiency

if props_loaded
    [~,obj.Excitation_Efficiency_Signal_LED1]=obj.spectr_multpl(filtered_LED1,obj.data.Fluophor1.Abs,obj.lambda_range);
    [~,obj.Excitation_Efficiency_Signal_LED2]=obj.spectr_multpl(filtered_LED2,obj.data.Fluophor1.Abs,obj.lambda_range);
    [~,obj.Excitation_Efficiency_Reference_LED1]=obj.spectr_multpl(filtered_LED1,obj.data.Fluophor2.Abs,obj.lambda_range);
    [~,obj.Excitation_Efficiency_Reference_LED2]=obj.spectr_multpl(filtered_LED2,obj.data.Fluophor2.Abs,obj.lambda_range);
    
    
end

%%%%% Bleed through - expressed as a fraction of total LED power
%%%%% overlapping with emitter spectdrum
%%%%% it doesn't inclued amount of back reflection, absolute powers etc.

if props_loaded
    [~,obj.LED1_bleed2signal]=obj.spectr_multpl(filtered_LED1,emitter1dichroics,obj.lambda_range);
    [~,obj.LED2_bleed2signal]=obj.spectr_multpl(filtered_LED2,emitter1dichroics,obj.lambda_range);
    
    [~,obj.LED1_bleed2reference]=obj.spectr_multpl(filtered_LED1,emitter2dichroics,obj.lambda_range);
    [~,obj.LED2_bleed2reference]=obj.spectr_multpl(filtered_LED2,emitter2dichroics,obj.lambda_range);
    
end


end