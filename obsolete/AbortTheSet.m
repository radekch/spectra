classdef AbortTheSet < handle
   properties (SetObservable, GetObservable, AbortSet)
      PropOne = 7
   end
   methods
      function obj = AbortTheSet(val)
         obj.PropOne = val;
         addlistener(obj,'PropOne','PreGet',@obj.getPropEvt);
         addlistener(obj,'PropOne','PreSet',@obj.setPropEvt);
      end
      function propval = get.PropOne(obj)
         disp('get.PropOne called')
         propval = obj.PropOne;
      end
      function set.PropOne(obj,val)
         disp('set.PropOne called')
         obj.PropOne = val;
      end
      function getPropEvt(obj,src,evnt)
         disp ('Pre-get event triggered')
         % ...
      end
      function setPropEvt(obj,src,evnt)
         disp ('Pre-set event triggered')
         % ...
      end
      function disp(obj)
         % Overload disp to avoid accessing property
         disp (class(obj))
      end
   end
end