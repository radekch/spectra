

clear('testsp')

%%

testsp=Spectra();


%
testsp.lambda_range=[380,750];

testsp.Fluophor1='GFP*';


testsp.Fluophor2='td*';


% testsp.Dichroic1='Di01-R488_561*';
testsp.Dichroic1='Alluxa3_45_ref*';
% testsp.Dichroic1='*89016*';
% testsp.Dichroic1='*8900*';
% testsp.Dichroic2='550 LP*';
% testsp.Dichroic2='550 LP*';
testsp.Dichroic2='FF555*';

% testsp.Exciter1='FF01-482_563*';
testsp.Exciter1='FF01-466*';
% testsp.Exciter1='*505_20*';
% testsp.Exciter1='*387*';
testsp.Exciter2='Bandpass-700*';



testsp.Emitter1='FF02-525_40*';
% testsp.Emitter1='FF02-529_24*';
% testsp.Emitter1='534-30*';

testsp.Emitter2='FF01-607*';
% testsp.Emitter2='*585-40*';
% testsp.Emitter2='*650_100*';


% testsp.LED1='*475nm*';
testsp.LED1='M470*';
% testsp.LED1='*505*';
% testsp.LED2='565 thorlabs*';
testsp.LED2='pulse*';

testsp
testsp.plot

%%

testsp=Spectra();
% testsp.Fluophor1='mNeon*';

testsp.Fluophor1='GFP*';


testsp.Fluophor2='td*';


testsp.Dichroic1='Di01-R488_561*';
% testsp.Dichroic1='Alluxa3_45_r*';

testsp.Dichroic2='550 LP*';

testsp.Exciter1='FF01-482_563*';
% testsp.Exciter1='*505_20*';
% testsp.Exciter1='*387*';



testsp.Emitter1='FF02-525_40*';
% testsp.Emitter1='FF02-529_24*';
% testsp.Emitter1='534-30*';

testsp.Emitter2='FF02-641_75*';
% testsp.Emitter2='*585-40*';
% testsp.Emitter2='*650_100*';


testsp.LED1='*475nm*';
% testsp.LED1='*505*';
testsp.LED2='565 thorlabs*';

testsp
testsp.plot


