% loadin spectrum data

load('mNeonGreeen')

%%
storeobj=Store('C:\Users\Radek\Google Drive\Postdoc\Progres reports\17-01-25 presentation 2')

%% Plot voltage indicator

plot(lem_mneong,em_mneong,lex_mneong,ex_mneong)

%% import gfp emission

filename = 'C:\Users\Radek\Google Drive\Postdoc\1p-2p scope\Spectra\GFP emission.txt';
delimiter = '\t';
startRow = 4;
formatSpec = '%f%f%[^\n\r]';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'HeaderLines' ,startRow-1, 'ReturnOnError', false);
fclose(fileID);

lgfp_em = dataArray{:, 1};
gfp_em = dataArray{:, 2};
lgfp_em=lgfp_em(1:(end-1));
gfp_em=gfp_em(1:(end-1));
gfp_em_norm=gfp_em/sum(gfp_em);
clearvars filename delimiter startRow formatSpec fileID dataArray ans;

plot(lgfp_em,gfp_em)
ylabel('Emission (a.u.)')
xlabel('\lambda (nm)')

%% import gfp absorbtion

filename = 'C:\Users\Radek\Google Drive\Postdoc\1p-2p scope\Spectra\GFP absorption.txt';
delimiter = '\t';
startRow = 4;
formatSpec = '%f%f%[^\n\r]';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'HeaderLines' ,startRow-1, 'ReturnOnError', false);
fclose(fileID);

lgfp_abs = dataArray{:, 1};
gfp_abs = dataArray{:, 2};
lgfp_abs=lgfp_abs(1:(end-1));
gfp_abs=gfp_abs(1:(end-1));
gfp_abs_norm=gfp_abs/sum(gfp_abs);
clearvars filename delimiter startRow formatSpec fileID dataArray ans;

plot(lgfp_abs,gfp_abs)
ylabel('Absorbtion (a.u.)')
xlabel('\lambda (nm)')

%% Overlay calcium voltage
clf
hold on
colors=linspecer(7,'qualitative')
%  colors=linspecer(2,'sequential')

plot(lgfp_abs,gfp_abs,'--','LineWidth',3, 'Color', colors(2,:))
plot(lgfp_em,gfp_em,'-','LineWidth',3, 'Color', colors(2,:))
plot(lem_mneong,em_mneong,'-','LineWidth',3, 'Color', colors(3,:))
plot(lex_mneong,ex_mneong,'--','LineWidth',3, 'Color', colors(3,:))
legend('GFP abs.','GFP em.','mNeonGreen abs.','mNeonGreen em.')
hold off
xlim([400,600])
ylabel('Intensity (a.u.)')
xlabel('Wavelength (nm)')

storeobj.splots('GFP_mNeonGreen')


% calcium only


clf
hold on
colors=linspecer(7,'qualitative')
%  colors=linspecer(2,'sequential')

plot(lgfp_abs,gfp_abs,'--','LineWidth',3, 'Color', colors(2,:))
plot(lgfp_em,gfp_em,'-','LineWidth',3, 'Color', colors(2,:))

legend('GFP abs.','GFP em.')
hold off
xlim([400,600])
ylabel('Intensity (a.u.)')
xlabel('Wavelength (nm)')
storeobj.splots('GFP')
