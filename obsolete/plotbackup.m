function plot(obj,varargin)
% plot(obj)
% plot(obj,list_of_properties_to_plot)
%plot(obj,'Dichroic1','Dichroic2') etc.

clf
if nargin==1
    list_of_properties_to_plot= {'Fluophor1', 'Fluophor2', 'LED1', 'LED2', 'Exciter1', 'Exciter2', 'Emitter1', 'Emitter2', 'Dichroic1', 'Dichroic2'};
elseif nargin>1
    list_of_properties_to_plot=varargin;
end

spectralist={};
idx=0;

for ii=1:numel(list_of_properties_to_plot)
    spectype=list_of_properties_to_plot{ii};
    
    if ~isempty(obj.(spectype))
        if strcmp(spectype(1:3),'Flu')==1
            obj.plot_spectr(obj.data.(spectype).Em);
            idx=idx+1;
            spectralist{idx}=[spectype,'-Em'];
            hold on
            obj.plot_spectr(obj.data.(spectype).Abs);
            idx=idx+1;
            spectralist{idx}=[spectype,'-Abs'];
        else
            obj.plot_spectr(obj.data.(spectype));
            idx=idx+1;
            spectralist{idx}=spectype;
            if ii>1
                hold on
            end
        end
    end
end
hold off
legend(spectralist)
xlim(obj.lambda_range)

end




