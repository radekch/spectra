%% Load spectra

tdTomato_abs=importfile('fluophor/tdTomato - Abs.txt');
tdTomato_em=importfile('fluophor/tdTomato - Em.txt');

mNeonG_abs=importfile('fluophor/mNeonGreen - Abs.txt');
mNeonG_em=importfile('fluophor/mNeonGreen - Em.txt');

dichroic1=importfile('dichroic/Di01-R488_561.txt');
dichroic2=importfile('dichroic/550 LP Omega optical.txt');

exciter=importfile('exciter/FF01-482_563.txt');

emitter1=importfile('emitter/FF02-525_40.txt');
% emitter1=importfile('emitter/FF02-529_24.txt');
emitter2=importfile('emitter/FF02-641_75.txt');

LED1=importfile('source/475nm LED (X-Cite TURBO).txt');
LED2=importfile('source/565 thorlabs.txt');


%% Plot current fluophors set

clf
plot_spectr(tdTomato_abs)
hold on
plot_spectr(tdTomato_em)
plot_spectr(mNeonG_abs)
plot_spectr(mNeonG_em)

xlim([400,725])

%% Plot current fluophors set

clf 
plot_spectr(exciter)
hold on
plot_spectr(dichroic1)
plot_spectr(dichroic2)

plot_spectr(emitter1)
plot_spectr(emitter2)

xlim([400,700])

%% LED  crop
clf
plot_spectr(LED2)
hold on
spectr_multpl(LED2,exciter)

xlim([400,700])

%% LED  crop
clf
plot_spectr(LED1)
hold on
spectr_multpl(LED1,exciter)

xlim([400,700])

%% overlap with emitter 1
clf
subplot(2,1,1)
plot_OD(exciter)
hold on
plot_OD(emitter1)
hold off
xlim([400,700])


subplot(2,1,2)
spectr_multpl(exciter,emitter1,[400,700])
xlim([400,700])

suptitle('Overlap of exciter with emitter 1')

%% overlap with emitter 2
clf
subplot(2,1,1)
plot_OD(exciter)
hold on
plot_OD(emitter2)
hold off
xlim([400,700])


subplot(2,1,2)
spectr_multpl(exciter,emitter2,[400,700])
xlim([400,700])


suptitle('Overlap of exciter with emitter 2')

%% Cross talk from tdTomato to mNeonGreen channel
clf
subplot(2,1,1)
spectr_multpl(tdTomato_em,emitter1)
xlim([400,700])
subplot(2,1,2)
spectr_multpl(mNeonG_em,emitter1)
xlim([400,700])
suptitle(sprintf('1: tdTomato cross talk to mNeon Green\n 2: Emitter overlap with mNeonGreen '))


%% Summary of properties


