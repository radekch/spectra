function output_spectrum=trans_multpl(spectrum1,spectrum2,varargin)
% output_spectrum=trans_multpl(spectrum1,spectrum2)
% output_spectrum=trans_multpl(spectrum1,spectrum2,range)
% multiplying transmission spectra 

dlambda=max(diff(spectrum1(1:2,1)),diff(spectrum2(1:2,1)));



if nargin>2
    range=varargin{1};
    lambda1=range(1);
    lambda2=range(2);
else
    % determining the overlap spectrum
    lambda1=max(spectrum1(1,1),spectrum2(1,1));
    lambda2=min(spectrum1(end,1),spectrum2(end,1));
end

npts=round((lambda2-lambda1)/dlambda);

lambda_vec=linspace(lambda1,lambda2,npts);

% preparing data for interpolation
spectrum1_new=interp1(spectrum1(:,1),spectrum1(:,2),lambda_vec,'linear','extrap');
spectrum2_new=interp1(spectrum2(:,1),spectrum2(:,2),lambda_vec,'linear','extrap');

output_spectrum=zeros(npts,2);

output_spectrum(:,1)=lambda_vec;
output_spectrum(:,2)=spectrum1_new.*spectrum2_new;



plot(output_spectrum(:,1),output_spectrum(:,2))
xlabel('\lambda (nm)')
ylabel('Multiplied spectra intensity')


