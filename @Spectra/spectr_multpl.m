function [output_spectrum,overlap]=spectr_multpl(spectrum_to_be_normalized_by_sum,spectrum_noramalized_to_1,varargin)
% output_spectrum=spectr_multpl(spectrum_to_be_normalized_by_sum,spectrum_noramalized_to_1)
% output_spectrum=spectr_multpl(spectrum_to_be_normalized_by_sum,spectrum_noramalized_to_1,range)
%
% output_spectrum - is not normalized! 
%
% Example applications:
% 1) Collection efficiency calculations:
% spectrum_to_be_normalized_by_sum - emission spectrum
% spectrum_noramalized_to_1 - transmission spectrum
%
% 2) Excitation efficiency calculations:
% spectrum_to_be_normalized_by_sum - LED spectrum
% spectrum_noramalized_to_1 - absorbtion spectrum 

% spectrum_to_be_normalized_by_sum and spectdrum2 - 2xN matrices, where the first column denotes
% wavelengths. They do not have to match in range. 

% checking if wavelenght vectors are the same
% TODO



dlambda=max(diff(spectrum_to_be_normalized_by_sum(1:2,1)),diff(spectrum_noramalized_to_1(1:2,1)));



% if nargin>2
%     it is causing problems ! Extrapolation instead of interpolation
%     range=varargin{1};
%     lambda1=range(1);
%     lambda2=range(2);
% else
    % determining the overlap spectrum
    lambda1=max(spectrum_to_be_normalized_by_sum(1,1),spectrum_noramalized_to_1(1,1));
    lambda2=min(spectrum_to_be_normalized_by_sum(end,1),spectrum_noramalized_to_1(end,1));
% end

npts=round((lambda2-lambda1)/dlambda);

lambda_vec=linspace(lambda1,lambda2,npts);

% preparing data for interpolation
spectrum_to_be_normalized_by_sum_new=interp1(spectrum_to_be_normalized_by_sum(:,1),spectrum_to_be_normalized_by_sum(:,2),lambda_vec,'linear','extrap');
spectrum_noramalized_to_1_new=interp1(spectrum_noramalized_to_1(:,1),spectrum_noramalized_to_1(:,2),lambda_vec,'linear','extrap');

% I believe I forgot the actual normalization 
spectrum_to_be_normalized_by_sum_new=spectrum_to_be_normalized_by_sum_new/sum(spectrum_to_be_normalized_by_sum_new);
% RCH 20/06/2017

output_spectrum=zeros(npts,2);

output_spectrum(:,1)=lambda_vec;
output_spectrum(:,2)=spectrum_to_be_normalized_by_sum_new.*spectrum_noramalized_to_1_new;

overlap=sum(output_spectrum(:,2))/sum(spectrum_to_be_normalized_by_sum_new);

% keyboard



plot(output_spectrum(:,1),output_spectrum(:,2))
xlabel('\lambda (nm)')
ylabel('Multiplied spectra intensity')
title(sprintf('Overlap=%g',overlap))

