function plot_OD(spectrum)

semilogy(spectrum(:,1),spectrum(:,2))
xlabel('\lambda (nm)')
ylabel('Optical density')