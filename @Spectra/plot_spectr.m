function plot_spectr(spectrum)

plot(spectrum(:,1),spectrum(:,2))
xlabel('\lambda (nm)')
ylabel('Intensity (a.u.)')