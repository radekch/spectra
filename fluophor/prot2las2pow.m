function [ grnchnlpow, redchnlpow, grnXpow, redXpow ] = prot2las2pow( l1wl, grnexco, grnqe, redexco, redqe, lpwr, grnspec, redspec, dich1spec, dich2spec, greenfilt, redfilt, grndet, reddet )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
%http://nic.ucsf.edu/dokuwiki/doku.php?id=fluorescent_proteins - list of
%fluorophore characteristics from UCSF
lbda = l1wl;
%%%%%%%%%%%%%% Parameters %%%%%%%%%%%%%%%%
% For 1 laser config: 488nm laser excitation only
% lbda=l1wl; %working wavelength

% This part calculate the emission energy wrt Ace concentration, given 200uW excitation
% for E0=200uW excitation, fluo of QE and absorption e over distance z can emit  QY.E0.10^(-e.z)
e_grn=grnexco; %in cm-1.M-1 27kDa=27kg/mol for Ace
QY_grn=grnqe; %quantum yield
e_red=redexco; % molecular weight 55kDa for tdTomato
QY_red=redqe;
redchnlgain = 10e6;
grnchnlgain = 10e7;

zlim=0.02;% z collection in cm
E0=lpwr*10e-6; % laser excitation power in W
C=1e-9; % molar concentration in mol/L: quasi random value here...
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Effective absoprtion coefficient for a given working wavelength:
%For 1 laser config: 488nm laser excitation only

ExcEff_grn=grnspec(lbda-400+1,2)/max(grnspec(:,2)); 
ExcEff_red=redspec(lbda-400+1,2)/max(redspec(:,2));

% Best emission energy expected: 
Eemit_grn=QY_grn*E0*(1-10.^(-e_grn*ExcEff_grn*zlim*C));
Eemit_red=QY_red*E0*(1-10.^(-e_red*ExcEff_red*zlim*C));

% Integral should be equal to expected emission Eemit_Ace
norm_Em_grn=(grnspec(:,3)./max(grnspec(:,3)))*ExcEff_grn; 
norm_Em_red=(redspec(:,3)./max(redspec(:,3)))*ExcEff_red;

% setup the fraction of transmission from 400nm to 800nm through     
% the green versus the red detector
ChG=dich1spec(:,2).*(1-dich2spec(:,2)).*greenfilt(:,2).*grndet(:,2);
ChR=dich1spec(:,2).*(dich2spec(:,2)).*redfilt(:,2).*reddet(:,2);

% calculate the spectrum for detection and cross-talk
Detected_grn=norm_Em_grn.*(ChG); 
Detected_red=norm_Em_red.*(ChR); 
XG_red=norm_Em_red.*ChG;
XR_grn=norm_Em_grn.*ChR;

grnchnlpow=sum(Detected_grn)/sum(norm_Em_grn)*Eemit_grn*grnchnlgain;%total energy detected from green sensor to green PR
redchnlpow=sum(Detected_red)/sum(norm_Em_red)*Eemit_red*redchnlgain;%total energy detected from red sensor to red PR
grnXpow=sum(XG_red)/sum(norm_Em_red)*Eemit_red*redchnlgain;%total energy detected from red sensor to green PR
redXpow=sum(XR_grn)/sum(norm_Em_grn)*Eemit_grn*grnchnlgain;%total energy detected from green sensor to red PR
end
