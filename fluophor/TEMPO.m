function varargout = TEMPO(varargin)
% TEMPO MATLAB code for TEMPO.fig
%      TEMPO, by itself, creates a new TEMPO or raises the existing
%      singleton*.
%
%      H = TEMPO returns the handle to a new TEMPO or the handle to
%      the existing singleton*.
%
%      TEMPO('CALLBACK',hObject,eventData,handles,...) calls the local041
%      function named CALLBACK in TEMPO.M with the given input arguments.
%
%      TEMPO('Property','Value',...) creates a new TEMPO or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before TEMPO_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to TEMPO_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TEMPO

% Last Modified by GUIDE v2.5 29-Nov-2017 15:13:40

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @TEMPO_OpeningFcn, ...
                   'gui_OutputFcn',  @TEMPO_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
%http://nic.ucsf.edu/dokuwiki/doku.php?id=fluorescent_proteins

% --- Executes just before TEMPO is made visible.
function TEMPO_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to TEMPO (see VARARGIN)

% Choose default command line output for TEMPO
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes TEMPO wait for user response (see UIRESUME)
% uiwait(handles.figure1);�
permstring = load('protvars.mat');
protstring = permstring.newstring;
set(handles.ProteinL1, 'String', protstring);
set(handles.ProteinL2, 'String', protstring);
set(handles.ProteinL3, 'String', protstring);
permstring2 = load('dichchoice.mat');
dichstring = permstring2.newstring;
set(handles.DC1spec, 'String', dichstring);
set(handles.DC2spec, 'String', dichstring);
set(handles.DC3spec, 'String', dichstring);
set(handles.DC4spec, 'String', dichstring);
set(handles.DC5spec, 'String', dichstring);
permstring3 = load('filtchoice.mat');
filtstring = permstring3.newstring;
set(handles.filt1, 'String', filtstring);
set(handles.filt2, 'String', filtstring);
set(handles.filt3, 'String', filtstring);
permstring4 = load('detspecchoice.mat');
detspecstring = permstring4.newstring;
set(handles.det1type, 'String', detspecstring);
set(handles.det2type, 'String', detspecstring);
set(handles.det3type, 'String', detspecstring);




% --- Outputs from this function are returned to the command line.
function varargout = TEMPO_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function L1_Wavelength_In_Callback(hObject, eventdata, handles)
% hObject    handle to L1_Wavelength_In (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of L1_Wavelength_In as text
%        str2double(get(hObject,'String')) returns contents of L1_Wavelength_In as a double


% --- Executes during object creation, after setting all properties.
function L1_Wavelength_In_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L1_Wavelength_In (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function L1_Power_In_Callback(hObject, eventdata, handles)
% hObject    handle to L1_Power_In (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of L1_Power_In as text
%        str2double(get(hObject,'String')) returns contents of L1_Power_In as a double


% --- Executes during object creation, after setting all properties.
function L1_Power_In_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L1_Power_In (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function L2_Wavelength_In_Callback(hObject, eventdata, handles)
% hObject    handle to L2_Wavelength_In (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of L2_Wavelength_In as text
%        str2double(get(hObject,'String')) returns contents of L2_Wavelength_In as a double


% --- Executes during object creation, after setting all properties.
function L2_Wavelength_In_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L2_Wavelength_In (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function L2_Power_In_Callback(hObject, eventdata, handles)
% hObject    handle to L2_Power_In (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of L2_Power_In as text
%        str2double(get(hObject,'String')) returns contents of L2_Power_In as a double


% --- Executes during object creation, after setting all properties.
function L2_Power_In_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L2_Power_In (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function L3_Wavelength_In_Callback(hObject, eventdata, handles)
% hObject    handle to L3_Wavelength_In (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of L3_Wavelength_In as text
%        str2double(get(hObject,'String')) returns contents of L3_Wavelength_In as a double


% --- Executes during object creation, after setting all properties.
function L3_Wavelength_In_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L3_Wavelength_In (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function L3_Power_In_Callback(hObject, eventdata, handles)
% hObject    handle to L3_Power_In (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of L3_Power_In as text
%        str2double(get(hObject,'String')) returns contents of L3_Power_In as a double


% --- Executes during object creation, after setting all properties.
function L3_Power_In_CreateFcn(hObject, eventdata, handles)
% hObject    handle to L3_Power_In (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in onephottgl.
function onephottgl_Callback(hObject, eventdata, handles)
% hObject    handle to onephottgl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of onephottgl


% --- Executes on button press in twophottgl.
function twophottgl_Callback(hObject, eventdata, handles)
% hObject    handle to twophottgl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of twophottgl


% --- Executes on button press in DC1spec.
function DC1spec_Callback(hObject, eventdata, handles)
% hObject    handle to DC1spec (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

permstring = load('dichchoice.mat'); %load variable with dichroic name
set(handles.DC1spec, 'String', permstring.newstring);
contents = cellstr(get(hObject,'String')); %get current dichroic names from the pull down menu
choices = size(contents);
dich = get(handles.DC1spec, 'Value'); %'Value' is a number that reffers to the number of the chosen option in the list

% figure out which option is chosen.  If the choice has been chosen before,
% print saved variable.  If 'Other' is chosen, ask for 2 column text file
% from user with columns labeled by 1 line.
for i = 1:choices
    if dich == i
       currchoice = contents{i}; 
       if currchoice == string('Other')
          specvect = uigetfile('*.txt', 'Select a .txt file'); %read file name from menu
          input = strtok(specvect,'.');
          disp(input)
          [dichdata] = funOpenSpectra(input, 2);
          plot(handles.DichSpectra,dichdata(:,1),dichdata(:,2));
          set(handles.dichplot,'String',input); %change title of plot to file name
          filename = char(strcat(input,'.mat'));
          save(filename, 'dichdata');
          newstring = [contents; input];
          permstring.newstring = newstring;
          save('dichchoice','newstring');
          set(handles.DC1spec, 'String', newstring)
       else
          currchar = char(strcat(currchoice, '.mat'));
          old = load(currchar);
          plot(handles.DichSpectra, old.dichdata(:,1), old.dichdata(:,2))
       end
    end

end


% --- Executes on button press in DC2spec.
function DC2spec_Callback(hObject, eventdata, handles)
% hObject    handle to DC2spec (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

permstring = load('dichchoice.mat');
set(handles.DC2spec, 'String', permstring.newstring);
contents = cellstr(get(hObject,'String'));
choices = size(contents);
dich = get(handles.DC2spec, 'Value');
for i = 1:choices
    if dich == i
       currchoice = contents{i}; 
       if currchoice == string('Other')
          specvect = uigetfile('*.txt', 'Select a .txt file'); %read file name from menu
          input = strtok(specvect,'.');
          disp(input)
          [dichdata] = funOpenSpectra(input, 2);
          plot(handles.DichSpectra,dichdata(:,1),dichdata(:,2));
          set(handles.dichplot,'String',input); %change title of plot to file name
          filename = char(strcat(input,'.mat'));
          save(filename, 'dichdata');
          newstring = [contents; input];
          permstring.newstring = newstring;
          save('dichchoice','newstring');
          set(handles.DC2spec, 'String', newstring)
       else
          currchar = char(strcat(currchoice, '.mat'));
          old = load(currchar);
          plot(handles.DichSpectra, old.dichdata(:,1), old.dichdata(:,2))
       end
    end

end

% --- Executes on button press in DC3spec.
function DC3spec_Callback(hObject, eventdata, handles)
% hObject    handle to DC3spec (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

permstring = load('dichchoice.mat');
set(handles.DC3spec, 'String', permstring.newstring);
contents = cellstr(get(hObject,'String'));
choices = size(contents);
dich = get(handles.DC3spec, 'Value');
for i = 1:choices
    if dich == i
       currchoice = contents{i}; 
       if currchoice == string('Other')
          specvect = uigetfile('*.txt', 'Select a .txt file'); %read file name from menu
          input = strtok(specvect,'.');
          disp(input)
          [dichdata] = funOpenSpectra(input, 2);
          plot(handles.DichSpectra,dichdata(:,1),dichdata(:,2));
          set(handles.dichplot,'String',input); %change title of plot to file name
          filename = char(strcat(input,'.mat'));
          save(filename, 'dichdata');
          newstring = [contents; input];
          permstring.newstring = newstring;
          save('dichchoice','newstring');
          set(handles.DC3spec, 'String', newstring)
       else
          currchar = char(strcat(currchoice, '.mat'));
          old = load(currchar);
          plot(handles.DichSpectra, old.dichdata(:,1), old.dichdata(:,2))
       end
    end

end

% --- Executes on button press in DC4spec.
function DC4spec_Callback(hObject, eventdata, handles)
% hObject    handle to DC4spec (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

permstring = load('dichchoice.mat');
set(handles.DC4spec, 'String', permstring.newstring);
contents = cellstr(get(hObject,'String'));
choices = size(contents);
dich = get(handles.DC4spec, 'Value');
for i = 1:choices
    if dich == i
       currchoice = contents{i}; 
       if currchoice == string('Other')
          specvect = uigetfile('*.txt', 'Select a .txt file'); %read file name from menu
          input = strtok(specvect,'.');
          disp(input)
          [dichdata] = funOpenSpectra(input, 2);
          plot(handles.DichSpectra,dichdata(:,1),dichdata(:,2));
          set(handles.dichplot,'String',input); %change title of plot to file name
          filename = char(strcat(input,'.mat'));
          save(filename, 'dichdata');
          newstring = [contents; input];
          permstring.newstring = newstring;
          save('dichchoice','newstring');
          set(handles.DC4spec, 'String', newstring)
       else
          currchar = char(strcat(currchoice, '.mat'));
          old = load(currchar);
          plot(handles.DichSpectra, old.dichdata(:,1), old.dichdata(:,2))
       end
    end

end

% --- Executes on button press in DC5spec.
function DC5spec_Callback(hObject, eventdata, handles)
% hObject    handle to DC5spec (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

permstring = load('dichchoice.mat');
set(handles.DC1spec, 'String', permstring.newstring);
contents = cellstr(get(hObject,'String'));
choices = size(contents);
dich = get(handles.DC5spec, 'Value');
for i = 1:choices
    if dich == i
       currchoice = contents{i}; 
       if currchoice == string('Other')
          specvect = uigetfile('*.txt', 'Select a .txt file'); %read file name from menu
          input = strtok(specvect,'.');
          disp(input)
          [dichdata] = funOpenSpectra(input, 2);
          plot(handles.DichSpectra,dichdata(:,1),dichdata(:,2));
          set(handles.dichplot,'String',input); %change title of plot to file name
          filename = char(strcat(input,'.mat'));
          save(filename, 'dichdata');
          newstring = [contents; input];
          permstring.newstring = newstring;
          save('dichchoice','newstring');
          set(handles.DC5spec, 'String', newstring)
       else
          currchar = char(strcat(currchoice, '.mat'));
          old = load(currchar);
          plot(handles.DichSpectra, old.dichdata(:,1), old.dichdata(:,2))
       end
    end

end

% --- Executes on selection change in det1type.
function det1type_Callback(hObject, eventdata, handles)
% hObject    handle to det1type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns det1type contents as cell array
%        contents{get(hObject,'Value')} returns selected item from det1type

permstring = load('detspecchoice.mat'); %load variable with dichroic name
set(handles.det1type, 'String', permstring.newstring);
contents = cellstr(get(hObject,'String')); %get current dichroic names from the pull down menu
choices = size(contents);
detspec = get(handles.det1type, 'Value'); %'Value' is a number that reffers to the number of the chosen option in the list

% figure out which option is chosen.  If the choice has been chosen before,
% print saved variable.  If 'Other' is chosen, ask for 2 column text file
% from user with columns labeled by 1 line in txt file.
for i = 1:choices
    if detspec == i
       currchoice = contents{i}; 
       if currchoice == string('Other')
          specvect = uigetfile('*.txt', 'Select a .txt file'); %read file name from menu
          input = strtok(specvect,'.');
          disp(input)
          [detspecdata] = funOpenSpectra(input, 2);
          plot(handles.SpecRespPlot,detspecdata(:,1),detspecdata(:,2));
          set(handles.detrespplot,'String',input); %change title of plot to file name
          filename = char(strcat(input,'.mat'));
          save(filename, 'detspecdata');
          newstring = [contents; input];
          permstring.newstring = newstring;
          save('detspecchoice','newstring');
          set(handles.det1type, 'String', newstring)
       else
          currchar = char(strcat(currchoice, '.mat'));
          old = load(currchar);
          plot(handles.SpecRespPlot, old.detspecdata(:,1), old.detspecdata(:,2))
       end
    end

end


% --- Executes during object creation, after setting all properties.
function det1type_CreateFcn(hObject, eventdata, handles)
% hObject    handle to det1type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in det2type.
function det2type_Callback(hObject, eventdata, handles)
% hObject    handle to det2type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns det2type contents as cell array
%        contents{get(hObject,'Value')} returns selected item from det2type


% --- Executes during object creation, after setting all properties.
function det2type_CreateFcn(hObject, eventdata, handles)
% hObject    handle to det2type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in det3type.
function det3type_Callback(hObject, eventdata, handles)
% hObject    handle to det3type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns det3type contents as cell array
%        contents{get(hObject,'Value')} returns selected item from det3type


% --- Executes during object creation, after setting all properties.
function det3type_CreateFcn(hObject, eventdata, handles)
% hObject    handle to det3type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function NoiseDet1_Callback(hObject, eventdata, handles)
% hObject    handle to NoiseDet1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of NoiseDet1 as text
%        str2double(get(hObject,'String')) returns contents of NoiseDet1 as a double


% --- Executes during object creation, after setting all properties.
function NoiseDet1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to NoiseDet1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function NoiseDet2_Callback(hObject, eventdata, handles)
% hObject    handle to NoiseDet2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of NoiseDet2 as text
%        str2double(get(hObject,'String')) returns contents of NoiseDet2 as a double


% --- Executes during object creation, after setting all properties.
function NoiseDet2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to NoiseDet2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function NoiseDet3_Callback(hObject, eventdata, handles)
% hObject    handle to NoiseDet3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of NoiseDet3 as text
%        str2double(get(hObject,'String')) returns contents of NoiseDet3 as a double


% --- Executes during object creation, after setting all properties.
function NoiseDet3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to NoiseDet3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in SpecResp1.
function SpecResp1_Callback(hObject, eventdata, handles)
% hObject    handle to SpecResp1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
specvect = uigetfile('*.txt', 'Select a .txt file'); %read file name from menu
data = load(specvect); %load two columns from file into array
xspecresp = data(:,1);
yspecresp = data(:,2);
plot(handles.SpecRespPlot,xspecresp,yspecresp); %plot spectum in ExcSpec plot (top right)
set(handles.flexcplot,'String',specvect); %change title of plot to file name


% --- Executes on button press in SpecResp2.
function SpecResp2_Callback(hObject, eventdata, handles)
% hObject    handle to SpecResp2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
specvect = uigetfile('*.txt', 'Select a .txt file'); %read file name from menu
data = load(specvect); %load two columns from file into array
xspecresp = data(:,1);
yspecresp = data(:,2);
plot(handles.SpecRespPlot,xspecresp,yspecresp); %plot spectum in ExcSpec plot (top right)
set(handles.flexcplot,'String',specvect); %change title of plot to file name


% --- Executes on button press in ShowSpec3.
function ShowSpec3_Callback(hObject, eventdata, handles)
% hObject    handle to ShowSpec3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
specvect = uigetfile('*.txt', 'Select a .txt file'); %read file name from menu
data = load(specvect); %load two columns from file into array
xspecresp = data(:,1);
yspecresp = data(:,2);
plot(handles.SpecRespPlot,xspecresp,yspecresp); %plot spectum in ExcSpec plot (top right)
set(handles.flexcplot,'String',specvect); %change title of plot to file name


% --- Executes on selection change in modfreq1.
function modfreq1_Callback(hObject, eventdata, handles)
% hObject    handle to modfreq1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns modfreq1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from modfreq1



% --- Executes during object creation, after setting all properties.
function modfreq1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to modfreq1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in modfreq2.
function modfreq2_Callback(hObject, eventdata, handles)
% hObject    handle to modfreq2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns modfreq2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from modfreq2


% --- Executes during object creation, after setting all properties.
function modfreq2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to modfreq2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in modfreq3.
function modfreq3_Callback(hObject, eventdata, handles)
% hObject    handle to modfreq3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns modfreq3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from modfreq3


% --- Executes during object creation, after setting all properties.
function modfreq3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to modfreq3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ProteinL1.
function ProteinL1_Callback(hObject, eventdata, handles)
% hObject    handle to ProteinL1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ProteinL1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ProteinL1
permstring = load('protvars.mat'); %load saved variable with protein names
set(handles.ProteinL1, 'String', permstring.newstring);
contents = cellstr(get(hObject,'String')); %get current options in pulldownmenu
choices = size(contents);
prot = get(handles.ProteinL1, 'Value'); % get user chosen value in the pop up list
superimp = get(handles.excspecsuper, 'Value'); % ask program if the superimpose buttom has been pressed
if superimp == 1
    hold on
else
    hold off
end

%if the chosen option has been saved previously, print spectra, qe and ec.
%If 'Other' chosen, upload text file with protein spectrum then ask user to
%input ec and qe
for i = 1:choices
    if prot == i
       currchoice = contents{i}; 
       if currchoice == string('Other')
          specvect = uigetfile('*.txt', 'Select a .txt file'); %read file name from menu
          input = strtok(specvect,'.');
          [saveddata] = funOpenSpectra(input, 3);
          plot(handles.ExcSpec,saveddata(:,1),saveddata(:,2)); %plot spectum in ExcSpec plot (top right)
          plot(handles.EmisSpec,saveddata(:,1),saveddata(:,3));
          set(handles.flexcplot,'String',input); %change title of plot to file name
          filename = char(strcat(input,'.mat'));
          newstring = [contents; input];
          permstring.newstring = newstring;
          save('protvars','newstring');
          set(handles.ProteinL1, 'String', newstring)
          titlebar = 'Enter Values:';
          default = {'0','0'};
          varprompt = {'Input Quantum Efficiency: ', 'Input Extinction Coefficient: '};
          userin = inputdlg(varprompt, titlebar, 1, default);
          qe = str2double(userin{1});
          ec = str2double(userin{2});
          save(filename, 'qe', 'ec', 'saveddata');
          set(handles.p1ec, 'String' , num2str(ec));
          set(handles.p1qe, 'String' , num2str(qe));
       else
          currchar = char(strcat(currchoice, '.mat'));
          old = load(currchar);
          plot(handles.ExcSpec, old.saveddata(:,1), old.saveddata(:,2));
          plot(handles.EmisSpec, old.saveddata(:,1), old.saveddata(:,3));
          set(handles.flexcplot,'String',currchoice);
          set(handles.flemiplot,'String',currchoice);
          set(handles.p1ec, 'String' , num2str(old.ec));
          set(handles.p1qe, 'String' , num2str(old.qe));
       end
    end

end



% --- Executes during object creation, after setting all properties.
function ProteinL1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ProteinL1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ProteinL2.
function ProteinL2_Callback(hObject, eventdata, handles)
% hObject    handle to ProteinL2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ProteinL2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ProteinL2
permstring = load('protvars.mat');
set(handles.ProteinL2, 'String', permstring.newstring);
contents = cellstr(get(hObject,'String'));
choices = size(contents);
prot = get(handles.ProteinL2, 'Value');
superimp = get(handles.excspecsuper, 'Value');
if superimp == 1
    hold on
else
    hold off
end
for i = 1:choices
    if prot == i
       currchoice = contents{i}; 
       if currchoice == string('Other')
          specvect = uigetfile('*.txt', 'Select a .txt file'); %read file name from menu
          input = strtok(specvect,'.');
          [saveddata] = funOpenSpectra(input, 3);
          plot(handles.ExcSpec,saveddata(:,1),saveddata(:,2)); %plot spectum in ExcSpec plot (top right)
          plot(handles.EmisSpec,saveddata(:,1),saveddata(:,3));
          set(handles.flexcplot,'String',input);
          set(handles.flemiplot,'String',input);%change title of plot to file name
          filename = char(strcat(input,'.mat'));
          newstring = [contents; input];
          permstring.newstring = newstring;
          save('protvars','newstring');
          set(handles.ProteinL1, 'String', newstring)
          titlebar = 'Enter Values:';
          default = {'0','0'};
          varprompt = {'Input Quantum Efficiency: ', 'Input Extinction Coefficient: '};
          userin = inputdlg(varprompt, titlebar, 1, default);
          qe = str2double(userin{1});
          ec = str2double(userin{2});
          save(filename, 'qe', 'ec', 'saveddata');
          set(handles.p2ec, 'String' , num2str(ec));
          set(handles.p2qe, 'String' , num2str(qe));
       else
          currchar = char(strcat(currchoice, '.mat'));
          old = load(currchar);
          plot(handles.ExcSpec, old.saveddata(:,1), old.saveddata(:,2));
          plot(handles.EmisSpec, old.saveddata(:,1), old.saveddata(:,3));
          set(handles.flexcplot,'String',currchoice);
          set(handles.flemiplot,'String',currchoice);
          set(handles.p2ec, 'String' , num2str(old.ec));
          set(handles.p2qe, 'String' , num2str(old.qe));
       end
    end
end


% --- Executes during object creation, after setting all properties.
function ProteinL2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ProteinL2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ProteinL3.
function ProteinL3_Callback(hObject, eventdata, handles)
% hObject    handle to ProteinL3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ProteinL3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ProteinL3
permstring = load('protvars.mat');
set(handles.ProteinL3, 'String', permstring.newstring);
contents = cellstr(get(hObject,'String'));
choices = size(contents);
prot = get(handles.ProteinL3, 'Value');
superimp = get(handles.excspecsuper, 'Value');
if superimp == 1
    hold on
else
    hold off
end
for i = 1:choices
    if prot == i
       currchoice = contents{i}; 
       if currchoice == string('Other')
          specvect = uigetfile('*.txt', 'Select a .txt file'); %read file name from menu
          input = strtok(specvect,'.');
          [saveddata] = funOpenSpectra(input, 3);
          plot(handles.ExcSpec,saveddata(:,1),saveddata(:,2)); %plot spectum in ExcSpec plot (top right)
          plot(handles.EmisSpec,saveddata(:,1),saveddata(:,3));
          set(handles.flexcplot,'String',specvect); %change title of plot to file name
          filename = char(strcat(input,'.mat'));
          newstring = [contents; input];
          permstring.newstring = newstring;
          save('protvars','newstring');
          set(handles.ProteinL1, 'String', newstring)
          titlebar = 'Enter Values:';
          default = {'0','0'};
          varprompt = {'Input Quantum Efficiency: ', 'Input Extinction Coefficient: '};
          userin = inputdlg(varprompt, titlebar, 1, default);
          qe = str2double(userin{1});
          ec = str2double(userin{2});
          save(filename, 'qe', 'ec', 'savewddata');
          set(handles.p3ec, 'String' , num2str(ec));
          set(handles.p3qe, 'String' , num2str(qe));
       else
          currchar = char(strcat(currchoice, '.mat'));
          old = load(currchar);
          plot(handles.ExcSpec, old.saveddata(:,1), old.saveddata(:,2));
          plot(handles.EmisSpec, old.saveddata(:,1), old.saveddata(:,3));
          set(handles.flexcplot,'String',currchoice);
          set(handles.flemiplot,'String',currchoice);
          set(handles.p3ec, 'String' , num2str(old.ec));
          set(handles.p3qe, 'String' , num2str(old.qe));
       end
    end
end


% --- Executes during object creation, after setting all properties.
function ProteinL3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ProteinL3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function dichplot_Callback(hObject, eventdata, handles)
% hObject    handle to dichplot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of dichplot as text
%        str2double(get(hObject,'String')) returns contents of dichplot as a double


% --- Executes during object creation, after setting all properties.
function dichplot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dichplot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function flexcplot_Callback(hObject, eventdata, handles)
% hObject    handle to flexcplot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of flexcplot as text
%        str2double(get(hObject,'String')) returns contents of flexcplot as a double


% --- Executes during object creation, after setting all properties.
function flexcplot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to flexcplot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function detrespplot_Callback(hObject, eventdata, handles)
% hObject    handle to detrespplot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of detrespplot as text
%        str2double(get(hObject,'String')) returns contents of detrespplot as a double


% --- Executes during object creation, after setting all properties.
function detrespplot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to detrespplot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function flemiplot_Callback(hObject, eventdata, handles)
% hObject    handle to flemiplot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of flemiplot as text
%        str2double(get(hObject,'String')) returns contents of flemiplot as a double


% --- Executes during object creation, after setting all properties.
function flemiplot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to flemiplot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when figure1 is resized.
function figure1_SizeChangedFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in excspecsuper.
function excspecsuper_Callback(hObject, eventdata, handles)
% hObject    handle to excspecsuper (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in Calculate.
function Calculate_Callback(hObject, eventdata, handles)
% hObject    handle to Calculate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%get all spectra needed for the calculation then run simulation.

choice1 = get(handles.DC1spec, 'Value');
contsdich1 = cellstr(get(handles.DC1spec,'String'));
[ dich1spec ] = getdichspec( choice1, contsdich1 );

choice2 = get(handles.DC2spec, 'Value');
contsdich2 = cellstr(get(handles.DC2spec,'String'));
[ dich2spec ] = getdichspec( choice2, contsdich2 );

% choice3 = get(handles.DC3spec, 'Value');
% contsdich3 = cellstr(get(handles.DC3spec,'String'));
% [ dich3spec ] = getdichspec( choice3, contsdich3 );
% 
% choice4 = get(handles.DC4spec, 'Value');
% contsdich4 = cellstr(get(handles.DC4spec,'String'));
% [ dich4spec ] = getdichspec( choice4, contsdich4 );
% 
% choice5 = get(handles.DC5spec, 'Value');
% contsdich5 = cellstr(get(handles.DC1spec,'String'));
% [ dich5spec ] = getdichspec( choice5, contsdich5 );

prot1 = get(handles.ProteinL1, 'Value');
contsprot1 = cellstr(get(handles.ProteinL1,'String'));
[ prot1spec ] = getprotspec( prot1, contsprot1 );

prot2 = get(handles.ProteinL2, 'Value');
contsprot2 = cellstr(get(handles.ProteinL2,'String'));
[ prot2spec ] = getprotspec( prot2, contsprot2 );

% prot3 = get(handles.ProteinL3, 'Value');
% contsprot3 = cellstr(get(handles.ProteinL3,'String'));
% [ prot3spec ] = getprotspec( prot3, contsprot3 );

filt1val = get(handles.filt1, 'Value');
contsfilt1 = cellstr(get(handles.filt1,'String'));
[ filt1spec ] = getfiltspec( filt1val, contsfilt1 );

filt2val = get(handles.filt2, 'Value');
contsfilt2 = cellstr(get(handles.filt2,'String'));
[ filt2spec ] = getfiltspec( filt2val, contsfilt2 );

% filt3val = get(handles.filt3, 'Value');
% contsfilt3 = cellstr(get(handles.filt3,'String'));
% [ filt3spec ] = getfiltspec( filt3val, contsfilt3 );

detspec1val = get(handles.det1type, 'Value');
contsdetspec1 = cellstr(get(handles.det1type,'String'));
[ detspec1 ] = getdetspec( detspec1val, contsdetspec1 );

detspec2val = get(handles.det2type, 'Value');
contsdetspec2 = cellstr(get(handles.det2type,'String'));
[ detspec2 ] = getdetspec( detspec2val, contsdetspec2 );

p1exco = str2double(get(handles.p1ec, 'String'));
p1qe = str2double(get(handles.p1qe, 'String'));
p2exco = str2double(get(handles.p2ec, 'String'));
p2qe = str2double(get(handles.p2qe, 'String'));

L1lambda = str2double(get(handles.L1_Wavelength_In, 'String'));
L1power = str2double(get(handles.L1_Power_In, 'String'));
L2lambda = str2double(get(handles.L2_Wavelength_In, 'String'));
L2power = str2double(get(handles.L2_Power_In, 'String'));

[ grnchnlpowl1, redchnlpowl1, grnXpowl1, redXpowl1 ] = prot2las2pow( L1lambda, p1exco, p1qe, p2exco, p2qe, L1power, prot1spec, prot2spec, dich1spec, dich2spec, filt1spec, filt2spec, detspec1, detspec2 );
set(handles.l1ch1p1, 'String', num2str(grnchnlpowl1));
set(handles.l1ch1p2, 'String', num2str(grnXpowl1));
set(handles.l1ch2p1, 'String', num2str(redXpowl1));
set(handles.l1ch2p2, 'String', num2str(redchnlpowl1));

[ grnchnlpowl2, redchnlpowl2, grnXpowl2, redXpowl2 ] = prot2las2pow( L2lambda, p1exco, p1qe, p2exco, p2qe, L2power, prot1spec, prot2spec, dich1spec, dich2spec, filt1spec, filt2spec, detspec1, detspec2 );
set(handles.l2ch1p1, 'String', num2str(grnchnlpowl2));
set(handles.l2ch1p2, 'String', num2str(grnXpowl2));
set(handles.l2ch2p1, 'String', num2str(redXpowl2));
set(handles.l2ch2p2, 'String', num2str(redchnlpowl2));

grnchnltot = grnchnlpowl1 + grnchnlpowl2 + grnXpowl1 + grnXpowl2;
redchnltot = redchnlpowl1 + redchnlpowl2 + redXpowl1 + redXpowl2;
set(handles.ch1tot, 'String', num2str(grnchnltot));
set(handles.ch2tot, 'String', num2str(redchnltot));

% --- Executes on selection change in filt1.
function filt1_Callback(hObject, eventdata, handles)
% hObject    handle to filt1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns filt1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from filt1
permstring = load('filtchoice.mat');
set(handles.filt1, 'String', permstring.newstring);
contents = cellstr(get(hObject,'String'));
choices = size(contents);
dich = get(handles.filt1, 'Value');
for i = 1:choices
    if dich == i
       currchoice = contents{i}; 
       if currchoice == string('Other')
          specvect = uigetfile('*.txt', 'Select a .txt file'); %read file name from menu
          input = strtok(specvect,'.');
          disp(input)
          [filtdata] = funOpenSpectra(input, 2);
          plot(handles.DichSpectra,filtdata(:,1),filtdata(:,2));
          set(handles.dichplot,'String',input); %change title of plot to file name
          filename = char(strcat(input,'.mat'));
          save(filename, 'filtdata');
          newstring = [contents; input];
          permstring.newstring = newstring;
          save('filtchoice','newstring');
          set(handles.filt1, 'String', newstring)
       else
          currchar = char(strcat(currchoice, '.mat'));
          old = load(currchar);
          plot(handles.DichSpectra, old.filtdata(:,1), old.filtdata(:,2))
       end
    end

end


% --- Executes during object creation, after setting all properties.
function filt1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to filt1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in filt2.
function filt2_Callback(hObject, eventdata, handles)
% hObject    handle to filt2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns filt2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from filt2
permstring = load('filtchoice.mat');
set(handles.filt2, 'String', permstring.newstring);
contents = cellstr(get(hObject,'String'));
choices = size(contents);
dich = get(handles.filt2, 'Value');
for i = 1:choices
    if dich == i
       currchoice = contents{i}; 
       if currchoice == string('Other')
          specvect = uigetfile('*.txt', 'Select a .txt file'); %read file name from menu
          input = strtok(specvect,'.');
          disp(input)
          [filtdata] = funOpenSpectra(input, 2);
          plot(handles.DichSpectra,filtdata(:,1),filtdata(:,2));
          set(handles.dichplot,'String',input); %change title of plot to file name
          filename = char(strcat(input,'.mat'));
          save(filename, 'filtdata');
          newstring = [contents; input];
          permstring.newstring = newstring;
          save('filtchoice','newstring');
          set(handles.filt2, 'String', newstring)
       else
          currchar = char(strcat(currchoice, '.mat'));
          old = load(currchar);
          plot(handles.DichSpectra, old.filtdata(:,1), old.filtdata(:,2))
       end
    end

end


% --- Executes during object creation, after setting all properties.
function filt2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to filt2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in filt3.
function filt3_Callback(hObject, eventdata, handles)
% hObject    handle to filt3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns filt3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from filt3
permstring = load('filtchoice.mat');
set(handles.filt3, 'String', permstring.newstring);
contents = cellstr(get(hObject,'String'));
choices = size(contents);
dich = get(handles.filt3, 'Value');
for i = 1:choices
    if dich == i
       currchoice = contents{i}; 
       if currchoice == string('Other')
          specvect = uigetfile('*.txt', 'Select a .txt file'); %read file name from menu
          input = strtok(specvect,'.');
          disp(input)
          [filtdata] = funOpenSpectra(input, 2);
          plot(handles.DichSpectra,filtdata(:,1),filtdata(:,2));
          set(handles.dichplot,'String',input); %change title of plot to file name
          filename = char(strcat(input,'.mat'));
          save(filename, 'filtdata');
          newstring = [contents; input];
          permstring.newstring = newstring;
          save('filtchoice','newstring');
          set(handles.filt3, 'String', newstring)
       else
          currchar = char(strcat(currchoice, '.mat'));
          old = load(currchar);
          plot(handles.DichSpectra, old.filtdata(:,1), old.filtdata(:,2))
       end
    end

end


% --- Executes during object creation, after setting all properties.
function filt3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to filt3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
