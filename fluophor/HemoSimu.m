% %% This part plot the effect of hemodynamics on effective laser intensity
% for various wavelength
I0=1;%mW
L=0.02; % in cm ie 200um

f0=12; % in Hz
t=0:0.001:1;
fHb=sin(2*pi*f0*t)';
fHbO2=sin(2*pi*f0*t+pi)';

lambda=[488 500 561 600 642 700]'; % in nm

eHbO2=zeros(length(lambda),1);
eHb=zeros(length(lambda),1);
I=zeros(length(t),length(lambda));

eHemoData=funOpenSpectra('HbO2HbSpectra',3); %lambda / HbO2 / Hb
% to convert these data (in cm-1/M-1) into absorption coeff (in cm-1), multiply by the molar
% concentration (typical is 150 gram of Hb/liter) and divide by the gram molecular
% weight=64500 g/mole (O2 negligeable). take care to not mix up 10^-a=exp(-2.303.a)
for i=1:length(lambda)
[idx,jdx]=find(eHemoData(:,1)==lambda(i,1));
eHbO2(i,1)=eHemoData(idx,2)*(150/64500); %in cm-1
eHb(i,1)=eHemoData(idx,3)*(150/64500);
I(:,i)=I0*10.^(-(fHbO2.*eHbO2(i,1)+fHb.*eHb(i,1)).*L);

figure(1)
plot(t,(I(:,i)-mean(I(:,i))), 'linewidth',2)
hold on
end
legend('488','500','561', '600','642','700')
ylim([-20 20])
hold off



%% Then, the detected photons modulated by hemodynamic
f0=12; % in Hz
Fs=1000;
t=0:1/Fs:1;
fHb=sin(2*pi*f0*t)';
fHbO2=sin(2*pi*f0*t+pi)';

eHemoData=funOpenSpectra('HbO2HbSpectra',3);

abs_HbO2=eHemoData(:,2)*(150/64500); %in cm-1
abs_Hb=eHemoData(:,3)*(150/64500);

HbO2Fluct=(Detected_Ace.*abs_HbO2)*fHbO2';
HbFluct=(Detected_Ace.*abs_Hb)*fHb';

I_Ace=10.^(-(sum(HbO2Fluct+HbFluct,1).*L))';

figure(3)
plot(t, I_Ace) % fluctuation of Ace emission
hold on
plot(t, I(:,1)) %fluctuation of 488nm laser
hold off



%% This part jsut plot the exctinction coeff wrt wavelength for HbO2 and Hb
eHemoData=funOpenSpectra('HbO2HbSpectra',3); %lambda / HbO2 / Hb

figure(3)
subplot(2,1,1)
plot(eHemoData(:,1),eHemoData(:,2),'b', 'linewidth',2)
hold on
plot(eHemoData(:,1),eHemoData(:,3),'r', 'linewidth',2)
hold off
xlim([400 800])
subplot(2,1,2)
semilogy(eHemoData(:,1),eHemoData(:,2)./eHemoData(:,3),'k', 'linewidth',2)
xlim([400 800])

% %% Power spectrum density plot
% Fs=1000;
% window = 10*Fs;
% overlap=9*Fs;
% Nfft=10*Fs;
% 
% [x,f] = pwelch(I_Ace,window,overlap,Nfft,Fs,'onesided');
% 
% figure(999)
% plot(f, 10*log(x))
% xlim([0 50])










