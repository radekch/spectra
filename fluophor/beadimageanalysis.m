
im = imread('exbeads3.jpg'); %read image
grayim = rgb2gray(im); %convert to greyscale
%binim = imbinarize(grayim); %convert greyscale to binary image 
binim = im2bw(grayim,0.9);
[x, y] = size(binim);
%cent = imregionalmax(binim); 
cents = regionprops(binim,'centroid'); %find centroids using imageprops
centroids = cat(1, cents.Centroid); %turn centoids into vector coords
imshow(binim); %show binary image
hold on
[rows cols] = size(centroids);
centcrop = zeros(rows,cols);
%gofxvec = zeros(1,1);
%gofyvec = zeros(1,1);
rsquarethresh = 0.8;
submatsize = 50;
for i = 1:rows %loop making sure only spots far enough from edge are used (indexing errors if spot too close to edge)
    if (centroids(i,1) > submatsize && centroids(i,1) < (x-submatsize))
        centcrop(i,1) = centroids(i,1);
    end
    if (centroids(i,2) > submatsize && centroids(i,2) < (y-submatsize))
        centcrop(i,2) = centroids(i,2);
    end
end
for i = 1:rows
    if centcrop(i,1) ~= 0 && centcrop (i,2) ~= 0
    centtemp = centcrop(i,:); %get one centoid from the vector
    centint = round(centtemp); %round to integer
    submat = grayim((centint(1,2)-submatsize):(centint(1,2)+submatsize), (centint(1,1)-submatsize):(centint(1,1)+submatsize)); %take submatrix of 80x80 pixels around chosen centroid
    submatbin = imbinarize(submat); %make submatrix binary
    submatcent = regionprops(submatbin,'centroid'); %find centoid of submat
    submatcents = cat(1, submatcent.Centroid); %put centroid in vector form
    xslice = im2double(submat(:,round(submatcents(1,2)))); %slice through centroid in x
    xslicex = linspace(1,size(xslice,1),size(xslice,1));
    yslice = im2double(submat(:,round(submatcents(1,1)))); %slice through centroid in y
    [curvex, gofx] = fit(xslicex.',xslice,'gauss1'); %fit xslice with 1D gauss
    [curvey, gofy] = fit(xslicex.',yslice,'gauss1'); %fit yslice with 1D gauss
    %f(x) =  a1*exp(-((x-b1)/c1)^2)
    if gofx.rsquare >= rsquarethresh && gofy.rsquare >= rsquarethresh
        %gofxvec = [gofxvec gofx.rsquare];
        %gofyvec = [gofyvec gofy.rsquare];
        plot(centroids(i,1),centroids(i,2), 'b*');
    end
    %figure
    %plot(curvex,xslicex.',xslice)
    %figure
    %plot(fy,xslicex.',xslice)
    end
end
hold off
