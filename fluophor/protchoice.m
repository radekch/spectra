function [ xexc,yexc,xem,yem,extcoef,qe ] = protchoice( prot )
%%following statements are queries about the chosen option and display the
%data correcponding to the option chosen.  If the user chooses "other", the
%program saves the data into a file that can later be added to Spectra.mat
%and put into the .fig as an option.
%   Detailed explanation goes here

data = load('Spectra.mat');
if (prot == 1)
    xexc = data.GFPabx;
    yexc = data.GFPaby;
    xem = data.GFPemx;
    yem = data.GFPemy;
end
if (prot == 2) 
    xexc = data.mNeonGreenabsx;
    yexc = data.mNeonGreenabsy;
    xem = data.mNeonGreenEmx;
    yem = data.mNeonGreenEmy;
    extcoef = '116000';
    qe = '0.80';
end
if (prot == 3)
    xexc = data.tdTomatoabsx;
    yexc = data.tdTomatoabsy;
    xem = data.tdTomatoemx;
    yem = data.tdTomatoemy;
    extcoef = '138000';
    qe = '0.69';
end

end

