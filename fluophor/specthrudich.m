function [ newprotpowx,newprotpowy ] = specthrudich( xdichspec,ydichspec,xprotspec,yprotspec )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
[xprotrows,xprotcols] = size(xprotspec);
newprotpowx = zeros(xprotrows,xprotcols);
newprotpowy = zeros(xprotrows,xprotcols);
for i = 1:xprotrows
    wl = find(xdichspec == xprotspec(i,1));
    powerout = ydichspec(wl,1)*yprotspec(i,1);
    newprotpowx(i,1) = xprotspec(i,1);
    newprotpowy(i,1) = powerout;
end

