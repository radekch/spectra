function [ x,y ] = DCchoice( choice )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes heredata1 = load('dichspecs.mat'); %load x and y spectral data for defaul dichroics

%currstring = get(handles.DC1spec, 'String');

%following statements are queries about the chosen option and display the
%data correcponding to the option chosen.  If the user chooses "other", the
%program saves the data into a file that can later be added to dichspecs
%and put into the .fig as an option.
data1 = load('dichspecs.mat'); %load x and y spectral data for defaut dichroics
if (choice == 1)
    x = data1.alluxax;
    y = data1.alluxay;
end
if (choice == 2) 
    x = data1.ff498di01x;
    y = data1.ff498di01y;
end
if (choice == 3)
    x = data1.ch89007x;
    y = data1.ch89007y;
end
if (choice == 4)
    x = data1.ch89016x;
    y = data1.ch89016y;
end
if (choice == 5)
    x = data1.di01r488x;
    y = data1.di01r488y;
end
if (choice == 6)
    x = data1.ff564di01x;
    y = data1.ff564di01y;
end
end

